const conversation = require('../api/conversation');
const { workspaces } = require('../config/api').conversation;

module.exports = {

    message: (req, res) => {
        conversation.message({
            input: { 
                text: req.body.message
            },
            workspace_id: workspaces[0]
         })
         .then((response) => {
            res.json(response);
         })
         .catch((error) => {
            console.log(error);
         });
    }

}