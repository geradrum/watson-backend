const corsMiddleware = require('./cors');
const parserMiddleware = require('./parser');

module.exports = (app) => {

    corsMiddleware(app);
    parserMiddleware(app);

};