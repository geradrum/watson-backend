const doAsync = require('doasync');
const Conversation = require('watson-developer-cloud/conversation/v1');

const configuration = require('../config/api');

const conversation = new Conversation({
    ...configuration.conversation,
    version_date: Conversation.VERSION_DATE_2017_05_26
});

module.exports = doAsync(conversation);

