const fs = require('fs');
const path = require('path');
const TextToSpeech = require('watson-developer-cloud/text-to-speech/v1');

const configuration = require('../config/api');

const textToSpeech = new TextToSpeech(configuration.textToSpeech);

const params = {
  text: 'Hello from IBM Watson API',
  voice: 'en-US_AllisonVoice',
  accept: 'audio/wav'
};

const output = path.join(process.env['HOME'], '/Downloads/output.wav');

textToSpeech.synthesize(params).pipe(fs.createWriteStream(output));