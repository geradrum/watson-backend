const express = require('express');

const middleware = require('./middleware');
const router = require('./router');
const { environment } = require('./config');

const app = express();

// Middleware
middleware(app);

//Router
router(app);

app.listen(environment.port, () => {
    console.log('Server listening at:', environment.port);
});