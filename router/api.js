const express = require('express');
const api = express.Router();

const chatController = require('../controllers/chat');

// Chat
api.route('/chat').post(chatController.message);


module.exports = api;